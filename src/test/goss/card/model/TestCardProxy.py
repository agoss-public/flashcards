'''
Created on Jun 11, 2010

@author: zarniwoop
'''
import unittest
from com.goss.card.model.card.CardHandler import CardHandler


class TestCardProxy(unittest.TestCase):

    def testName(self):
        proxy = CardHandler()
        category = "Bible Verses"
        proxy.populateCards(category)
        
        self.assertTrue("No cards found!", len(proxy.cards.keys()) > 0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
