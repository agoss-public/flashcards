'''
Created on May 13, 2010
@author: zarniwoop
'''
import sys

from PyQt5 import QtWidgets

from src.com.goss.card.view.main.CardLauncher import CardLauncher

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    widget = QtWidgets.QWidget()

    cardLauncher = CardLauncher(widget)
    app.exec_()
