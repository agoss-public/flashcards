'''
Created on Jun 13, 2010

@author: zarniwoop
'''
from com.goss.card.model.card.db.DBConnection import DBConnection


class CardVO():
    (IMAGE, TEXT) = range(0, 2)
    '''
    classdocs
    '''

    def __init__(self, category, bucket=None, displayedTime=None, interval=None, cardType=None,
                 question=None, qFormat=None, hint=None, answer=None, aFormat=None):
        self.category = category
        self.bucket = bucket
        self.displayedTime = displayedTime
        self.interval = interval
        self.cardType = cardType
        self.question = question
        self.hint = hint
        self.qFormat = qFormat
        self.answer = answer
        self.aFormat = aFormat

    def getImage(self):
        if self.cardType == CardVO.TEXT:
            raise TypeError("Cannot fetch image for a TEXT card")
        else:
            query = "SELECT name,path FROM images,flashcards WHERE ", \
                    "flashcards.image_name==images.name"
            conn = DBConnection.getConnection(DBConnection.ConnectionTypes.SQLITE_CONN)
            cursor = conn.cursor()
            cursor.execute(query)
