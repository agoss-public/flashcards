'''
Created on May 2, 2010

@author: Aaron Goss
'''
from com.goss.card.model.card.db.DBConnection import DBConnection
from threading import Timer

TIMER_INCR = 6000

class QueryManager():

    def __init__(self, incr=TIMER_INCR):
        '''
        Constructor
        '''
        self.connection = DBConnection.getConnection(DBConnection.ConnectionTypes.MYSQL_CONN)
        self.timer = Timer(incr, self.checkDB)
        self.lastAccessed = 0

    def startTimer(self):
        if not self.timer.isAlive():
            self.timer.start()
    
    def endTimer(self):
        if self.timer.isAlive():
            self.timer.cancel()
            
    def checkDB(self):
        """
        Every increment, this method checks the database to see if there are any cards which need to be passed along to the
        user for guessing.
        """
        pass