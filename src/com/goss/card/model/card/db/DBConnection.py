'''
Created on May 2, 2010

@author: Aaron Goss
'''
import os
import sqlite3

DEFAULT_HOST = "https://beeblebrox"
SQLITE_DB = os.path.split(__file__)[0] + "/../../../db/flashcards.db"


class DBConnection():
    class ConnectionTypes():
        MYSQL_CONN, SQLITE_CONN = range(0, 2)

    @staticmethod
    def getConnection(type, user="", password="", database="", host=DEFAULT_HOST):
        if type == DBConnection.ConnectionTypes.SQLITE_CONN:
            return sqlite3.connect(SQLITE_DB)
