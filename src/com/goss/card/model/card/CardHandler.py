'''
Created on Mar 20, 2010

@author: zarniwoop
'''
import time

from com.goss.card.model.card.CardVO import CardVO
from com.goss.card.model.card.db.DBConnection import DBConnection


class CardHandler():
    '''
    classdocs
    '''

    def __init__(self):
        self.cards = {}
        self.cardIter = None

    def populateCards(self, category):
        conn = DBConnection.getConnection(DBConnection.ConnectionTypes.SQLITE_CONN)
        cursor = conn.cursor()
        curTime = time.time()

        query = ''.join(["SELECT fl.category,fl.bucket_id,fl.last_displayed_time,fl.card_type,bk.display_interval,"
                         "fl.question,fl.question_format,fl.hint,fl.answer,fl.answer_format ",
                         "FROM flashcards fl, categories ct, buckets bk, formats fmt ",
                         "WHERE fl.category == ?",
                         " AND fl.bucket_id == bk.id"
                         " AND (fl.last_displayed_time + bk.display_interval) <= ?",
                         " AND fl.question_format == fmt.format",
                         " AND fl.answer_format == fmt.format",
                         " ORDER BY fl.bucket_id ASC"])

        cursor.execute(query, (category, curTime))
        #        cursor.execute("SELECT category,bucket_id,last_displayed_time FROM flashcards,buckets WHERE category == ?"
        #                       " AND flashcards.bucket_id==buckets.id"
        #                       " AND (flashcards.last_displayed_time - buckets.display_interval) <= ?"
        #                       " ORDER BY flashcards.bucket_id ASC", (category, curTime))

        # First, flush the current set of cards
        self.cards = {}
        for card in cursor:
            self.cards[str(card[4])] = CardVO(card)

        cursor.close()
        conn.close()

    def getNextCard(self, category=""):
        if self.cardIter is None:
            self.cardIter = iter(self.cards.keys())

        try:
            return self.cards[self.cardIter.next()].answer
        except StopIteration:
            self.cardIter = None
            return self.getNextCard(category)

    def getAnswer(self, question):
        return self.cards[question]

    def getHint(self, question):
        pass

    def getCategories(self):
        conn = DBConnection.getConnection(DBConnection.ConnectionTypes.SQLITE_CONN)
        cursor = conn.cursor()
        cursor.execute("SELECT name, question from categories, flashcards where flashcards.category == categories.name")
        categories = []
        for category in cursor:
            categories.append({str(category[0]): str(category[1])})

        return categories

    def getDates(self):
        conn = DBConnection.getConnection(DBConnection.ConnectionTypes.SQLITE_CONN)
        cursor = conn.cursor()
        cursor.execute(
            "SELECT last_displayed_time, display_interval FROM flashcards, buckets WHERE flashcards.bucket_id == buckets.id")
