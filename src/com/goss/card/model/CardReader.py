'''
Created on Mar 20, 2010

@author: zarniwoop
'''
import csv

class CardReader():

    def __init__(self, file, delimiter=','):
        self.fileH = open(file)
        self.delimiter = delimiter
        self.reader = self.readFile(self.fileH, self.delimiter)
    
    def next(self):
        '''
        Returns the next element in the CSV file; if we've hit the end of the
        file, start over and return the first.
        '''
        nextLine = self.reader.next()

        if nextLine is None:
            self.reader = self.readFile(self.fileH, self.delimiter)
            nextLine      = self.reader.next()
        return nextLine
    
    def close(self):
        '''
        Closes the file handle so we can ignore the rest of the file
        '''
        self.fileH.close()

    def readFile(self, file, delim):
        return csv.reader(file, delim)