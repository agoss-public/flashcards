-- DROP ALL TABLES and INDICES so we start with a clean slate
drop table if exists buckets;
drop table if exists images;
drop table if exists flashcards;
drop table if exists formats;
drop table if exists categories;

drop index if exists display_time_idx;
drop index if exists display_intvl_idx;
drop index if exists bucket_idx;
drop index if exists category_idx;


-- TABLE TO MAP CARDS TO A "BUCKET".  Buckets associate cards together
-- with a time interval in which they will be displayed.  Each card is
-- displayed as a function of their last read time and the interval their
-- bucket is associated with.
-- Buckets may also define their own number of tries in which a card may
-- advance to a new bucket (and thus a new interval)
create table buckets (
      id INTEGER PRIMARY KEY NOT NULL,
      display_interval TIMESTAMP NOT NULL DEFAULT 3600000,
      attempts_to_advance INTEGER NOT NULL DEFAULT 5
);

-- TABLE TO MAP IMAGES TO A LOCATION ON THE HDD.  Cards may either be
-- text (regardless of format) or image data.  We won't store the image
-- in the database -- it's a waste of space.  Users may define their own
-- loications for images if they wish.
create table images (
       name VARCHAR(10) PRIMARY KEY NOT NULL,
       path VARCHAR(255) NOT NULL
);

create table categories (
       name VARCHAR(20) PRIMARY KEY NOT NULL
);

-- PRIMARY ACCESSED TABLE TO REPRESENT CARDS.  These cards have several
-- attributes:
--  * bucket: a bucket/interval when they will be displayed
--  * last_access_time: a timestamp when the card was last displayed
--  * card_type: either an image or a text-based input 0 or 1
--  * question: the question to prompt the user with: if an image, we will
--    		rely on the path to grab the card
--  * answer: the answer to the question.  This is always text.
--  * answer_fmt
create table flashcards (
       id INTEGER PRIMARY KEY NOT NULL,
       category VARCHAR(20),
       bucket_id INTEGER NOT NULL,
       last_displayed_time TIMESTAMP NOT NULL,
       card_type BOOLEAN NOT NULL DEFAULT 0,
       question VARCHAR(255) NOT NULL,
       hint VARCHAR(255),
       question_format VARCHAR(10) NOT NULL,
       answer VARCHAR(255) NOT NULL,
       answer_format VARCHAR(10),
       image_name VARCHAR(10),

       foreign key(bucket_id) references buckets(name),
       foreign key(image_name) references images(name),
       foreign key(answer_format) references formats(format),
       foreign key(question_format) references formats(format),
       foreign key(category) references category(name)
);

create table formats (
       format VARCHAR(10) NOT NULL PRIMARY KEY
);

create index display_time_idx on flashcards(last_displayed_time);
create index category_idx on flashcards(category);
create index bucket_idx on flashcards(bucket_id);
create index display_intvl_idx on buckets(display_interval);

-- Setup the default values
insert into formats(format) values('plaintext');
insert into formats(format) values('greek');
insert into buckets(display_interval) values(3600000);
insert into buckets(display_interval) values(7200000);

-- Setup the flashcards
insert into flashcards(bucket_id, category, last_displayed_time, question, answer, answer_format, question_format) values (1, "Bible Verses", 0, "Romans 11:33-36", "Oh the depth both of the wisdom and knowledge of God!  How unsearchable are His judgments and unfathomable His ways!  For 'Who has known the mind of the Lord, or who has become His counselor?' Or 'Who has first given to Him that it might be paid back to him again?' For from Him and through Him and to Him are all things. To Him be the glory forever. Amen", "plaintext", "plaintext");

insert into flashcards(bucket_id, category, last_displayed_time, question, answer, answer_format, question_format) values (2, "Bible Verses", 0, "John 3:16", "For God so love the world, that He gave His only begotten Son, that whoever believes in Him shall not perish, but have eternal life", "plaintext", "plaintext");

insert into categories(name) values("Bible Verses");
insert into categories(name) values("Greek Endings");
insert into categories(name) values("Books of the Bible");