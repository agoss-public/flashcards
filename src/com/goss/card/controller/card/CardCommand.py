'''
Created on Mar 21, 2010

@author: Aaron Goss
'''
from com.goss.card.model.card.CardHandler import CardHandler
class CardCommand():
    def __init__(self):
        self.cardHandler = CardHandler()
    
    def populateCards(self, category):
        self.cardHandler.populateCards(category)
    
    def getNextCard(self):
        return self.cardHandler.getNextCard()