'''
Created on Apr 3, 2010

@author: Aaron Goss
'''
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from com.goss.card.view.card.output.CardOutputFrame import CardOutputFrame
from com.goss.card.view.card.input.CardInputFrame import CardInputFrame
from com.goss.card.controller.card.CardCommand import CardCommand

CONFIRM = "&Confirm"
NEXT = "&Next"
HINT = "&Hint"
CEDE = "Ced&e"


class CardUIFrame(QtWidgets.QWidget):

    def __init__(self, *args, **kargs):
        '''
        Constructor
        '''
        QtWidgets.QWidget.__init__(self)
        self.command = CardCommand()
        self.command.populateCards("Bible Verses")
        layout = QtWidgets.QHBoxLayout()

        self.createCardOutput(layout)
        self.createCardInput(layout)

        self.setLayout(layout)

    def createCardOutput(self, layout):
        self.displayFrame = CardOutputFrame()
        layout.addWidget(self.displayFrame)

    def createCardInput(self, layout):
        self.inputFrame = CardInputFrame()
        self.inputFrame.nextButton.clicked.connect(self.getNextCard)
        layout.addWidget(self.inputFrame)

    @QtCore.pyqtSlot()
    def getNextCard(self):
        card = self.command.getNextCard()
        self.displayFrame.cardDisplay.setText(card)
