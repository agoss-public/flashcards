'''
Created on May 1, 2010

@author: Aaron Goss
'''
from PyQt5 import QtWidgets


class TextInput(QtWidgets.QWidget):
    class TextTypes():
        LINE_EDIT, MULTILINE = range(0, 2)

    def __init__(self):
        '''
        Constructor
        '''
        QtWidgets.QWidget.__init__(self)

        self.setLayout(QtWidgets.QVBoxLayout())

        self.input = QtWidgets.QLineEdit()
        self.inputType = TextInput.TextTypes.LINE_EDIT
        self.layout().addWidget(self.input)

    def swapInputType(self):
        if self.inputType == TextInput.TextTypes.LINE_EDIT:
            text = self.input.text()
            self.setInputType(TextInput.TextTypes.MULTILINE)
            self.inputType = TextInput.TextTypes.MULTILINE
            self.input.setPlainText(text)
        else:
            text = self.input.toPlainText()
            self.setInputType(TextInput.TextTypes.LINE_EDIT)
            self.inputType = TextInput.TextTypes.LINE_EDIT
            self.input.setText(text)

    def setInputType(self, inputType):
        # remove the input if it's already there...
        if self.layout().indexOf(self.input) != -1:
            self.layout().removeWidget(self.input)

        if inputType == TextInput.TextTypes.LINE_EDIT:
            self.input = QtWidgets.QLineEdit()
        else:
            self.input = QtWidgets.QPlainTextEdit()

        self.layout().addWidget(self.input)

    def getText(self):
        if self.inputType == TextInput.TextTypes.LINE_EDIT:
            return self.input.text()
        else:
            return self.input.document.toPlainText()

    def setText(self, text):
        if self.inputType == TextInput.TextTypes.LINE_EDIT:
            self.input.setText(text)
        else:
            self.input.setPlainText(text)
