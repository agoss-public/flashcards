'''
Created on Mar 23, 2010

@author: zarniwoop
'''
from PyQt5 import QtCore, QtWidgets


class CardInputMediator():
    
    def __init__(self):
        self.setupConnections()
    
    def setupConnections(self):
        """
        """
        self.mainWindow().nextButton.clicked.connect(self.getNextCard)
        self.mainWindow().confirmButton.clicked.connect(self.confirmCard)
        self.mainWindow().cedeButton.clicked.connect(self.cedeCard)
        self.mainWindow().hintButton.clicked.connect(self.getHint)
        self.mainWindow().textInput.returnPressed.connect(self.confirmCard)
        self.mainWindow().textInput.textChanged.connect(self.checkText)
        
    @QtCore.pyqtSlot()
    def getNextCard(self):
        pass
    
    @QtCore.pyqtSlot()
    def getHint(self):
        pass
    
    @QtCore.pyqtSlot()
    def cedeCard(self):
        pass
    
    @QtCore.pyqtSlot()
    def confirmCard(self):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setText("Guess!")
        msgBox.exec_()
        pass

    def showHint(self, hint):
        self.viewComponent.updateHint(hint)
    
    @QtCore.pyqtSlot()
    def updateCard(self, card):
        self.viewComponent.updateCard(card)
    
    def updateUserMessage(self, message):
        self.viewComponent.updateUserMessage(message)
    
    def checkText(self, text):
        print(text)
    
    def mainWindow(self):
        return self.viewComponent