'''
Created on May 1, 2010

@author: Aaron Goss
'''
from PyQt5 import QtWidgets

CONFIRM = "&Confirm"
NEXT = "&Next"
HINT = "&Hint"
CEDE = "Ce&de"


class CardInputFrame(QtWidgets.QWidget):

    def __init__(self):
        """
        Constructor
        """
        QtWidgets.QWidget.__init__(self)
        layout = QtWidgets.QVBoxLayout()

        self.createTextInput(layout)
        self.createControls(layout)

        self.setLayout(layout)

    def createTextInput(self, layout):
        frame = QtWidgets.QFrame()
        frameLayout = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel()
        label.setText("Your Guess:")

        self.textInput = QtWidgets.QLineEdit()
        #        self.textInput = TextInput()
        #        self.expandButton = QPushButton()
        #        self.expandButton.setText("E")
        #        self.expandButton.setFixedWidth(20)
        #        self.connect(self.expandButton, QtCore.SIGNAL("released()"), self.expandText)

        frameLayout.addWidget(label)
        frameLayout.addWidget(self.textInput)
        #        frameLayout.addWidget(self.expandButton)
        frame.setLayout(frameLayout)

        layout.addWidget(frame)

    def createControls(self, layout):
        '''
        createControls()
        
        Parameters:
            - layout: the layout controls will be added to
        
        This function takes in a layout object which is used
        to contain controls
        '''
        box = QtWidgets.QFrame()
        gridLayout = QtWidgets.QGridLayout()
        self.confirmButton = QtWidgets.QPushButton(CONFIRM)
        self.nextButton = QtWidgets.QPushButton(NEXT)
        self.hintButton = QtWidgets.QPushButton(HINT)
        self.cedeButton = QtWidgets.QPushButton(CEDE)

        gridLayout.addWidget(self.confirmButton, 1, 0, 1, 1)
        gridLayout.addWidget(self.nextButton, 1, 1, 1, 1)
        gridLayout.addWidget(self.hintButton, 2, 0, 1, 1)
        gridLayout.addWidget(self.cedeButton, 2, 1, 1, 1)
        box.setLayout(gridLayout)
        layout.addWidget(box)

    def expandText(self):
        self.textInput.swapInputType()
