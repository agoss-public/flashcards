'''
Created on Apr 3, 2010

@author: Aaron Goss
'''
from PyQt5 import QtWidgets


class CardOutputFrame(QtWidgets.QWidget):

    def __init__(self, *args, **kwargs):
        '''
        Constructor
        '''
        QtWidgets.QWidget.__init__(self)
        layout = QtWidgets.QVBoxLayout()

        self.createCardDisplay(layout)
        self.createCardPrompt(layout)

        self.setLayout(layout)

    def createCardDisplay(self, layout):
        '''
        '''
        self.cardDisplay = QtWidgets.QLabel()
        self.cardDisplay.setText("Card: ")

        layout.addWidget(self.cardDisplay)

    def createCardPrompt(self, layout):
        '''
        '''
        self.cardPrompt = QtWidgets.QLabel()
        self.cardPrompt.setText("Hint: ")
        layout.addWidget(self.cardPrompt)
