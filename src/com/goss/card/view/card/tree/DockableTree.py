'''
Created on May 29, 2010

@author: Aaron Goss
'''
from PyQt5 import QtCore, QtWidgets, QtGui
from com.goss.card.model.card.CardHandler import CardHandler


class DockableTree(QtWidgets.QDockWidget):

    def __init__(self, *args, **kwargs):
        QtWidgets.QDockWidget.__init__(self)
        self.setFeatures(QtWidgets.QDockWidget.AllDockWidgetFeatures)
        self.RIGHT_ICON = QtGui.QIcon("/home/zarniwoop/workspace/Flashcards/libs/right-arrow.png")
        self.LEFT_ICON = QtGui.QIcon("/home/zarniwoop/workspace/Flashcards/libs/left-arrow.png")

        self.RIGHT_ICON.actualSize(QtCore.QSize(4, 4))
        self.LEFT_ICON.actualSize(QtCore.QSize(4, 4))

        self.proxy = CardHandler()

        self.tree = QtWidgets.QTreeWidget(self)
        self.frameWidget = self.createFrameWidget()
        self.setWidget(self.frameWidget)

        self.addTreeItems(self.proxy.getCategories())

    def createFrameWidget(self):
        frame = QtWidgets.QFrame(self)
        self.button = QtWidgets.QPushButton()

        self.button.setIcon(self.LEFT_ICON)
        self.button.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.button.clicked.connect(self.resizeWidget)

        layout = QtWidgets.QVBoxLayout()

        layout.addWidget(self.button)
        layout.addItem(QtWidgets.QSpacerItem(1, 1, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed))
        layout.addWidget(self.tree)

        frame.setLayout(layout)
        return frame

    def addTreeItems(self, categories):
        self.tree.setColumnCount(1)
        self.tree.setHeaderLabels(["Categories"])
        items = []
        for category in categories:
            for key, val in category.items():
                categoryItem = QtWidgets.QTreeWidgetItem(self.tree, 0)
                categoryItem.setChildIndicatorPolicy(QtWidgets.QTreeWidgetItem.ShowIndicator)
                categoryQues = QtWidgets.QTreeWidgetItem(categoryItem, 0)
                categoryItem.setText(0, key)
                categoryQues.setText(0, val)
                categoryItem.addChild(categoryQues)

                items.append(categoryItem)

        self.tree.insertTopLevelItems(0, items)

    @QtCore.pyqtSlot()
    def resizeWidget(self):
        if self.tree.isVisible():
            self.tree.setVisible(False)
            self.button.setIcon(self.RIGHT_ICON)
        else:
            self.tree.setVisible(True)
            self.button.setIcon(self.LEFT_ICON)
