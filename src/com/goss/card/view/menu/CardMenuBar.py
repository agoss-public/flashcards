'''
Created on Apr 2, 2010

@author: Aaron Goss
'''
from PyQt5 import QtWidgets, QtCore


class CardMenuBar(QtWidgets.QMenuBar):

    def __init__(self, *args, **kargs):
        '''
        Constructor
        '''
        QtWidgets.QMenuBar.__init__(self)
        self.addMenu("C&ards")
        self.addMenu("C&onfig")
        self.addMenu("H&elp")

    def registerMenuOption(self, parentMenu, menuTitle, menuMethod):
        '''
        registerMenuOption()
        
        Paramters:
            - parentMenu:   a QWidget which will have the new QMenu
            - menuTitle:    the text-label for the new QMenu
            - menuMethod:   the method which will be executed when this
                            QMenu is selected
        
        This method creates a new QMenu object, associated with provisioned
        parameters.  These parameters define where the menu item is placed in
        this toolbar, as well as what the menu item is titled, and what happens
        when the user selects it.  This menu does not know anything other than
        the method to invoke when the user clicks it.
        '''
        newMenu = QtWidgets.QMenu(menuTitle, parentMenu)
        if parentMenu is self:
            self.addMenu(newMenu)
        if parentMenu is QtWidgets.QMenu:
            parentMenu.addMenu(newMenu)

        self.connect(newMenu, QtCore.SIGNAL("clicked()"), menuMethod)

    def unregisterMenuOption(self, parentMenu, menuTitle):
        '''
        '''
