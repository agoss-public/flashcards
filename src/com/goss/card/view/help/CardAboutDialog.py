'''
Created on May 28, 2010

@author: Aaron Goss
'''
from PyQt5 import QtWidgets, QtGui


class CardAboutDialog(QtWidgets.QDialog):

    def __init__(self, parent):
        """
        Displays a simple about dialog for quick information about this app
        """
        QtWidgets.QDialog.__init__(self, parent)
        self.setWindowTitle("About: Re-Collections")

        layout = QtWidgets.QGridLayout(parent)

        scene = QtWidgets.QGraphicsScene(parent)
        view = QtWidgets.QGraphicsView(scene)

        image = QtGui.QPixmap("/home/zarniwoop/workspace/Flashcards/libs/icon.png")
        scene.addPixmap(image)

        layout.addWidget(view, 0, 0)

        contentFrame = QtWidgets.QFrame()
        contentLayout = QtWidgets.QVBoxLayout()

        contentLayout.addWidget(QtWidgets.QLabel("Re-Collections"))
        contentLayout.addWidget(QtWidgets.QLabel("Author:  Aaron Goss"))
        contentLayout.addWidget(QtWidgets.QLabel("Date:    2010"))
        contentLayout.addWidget(QtWidgets.QLabel("Version: 1.0"))

        contentFrame.setLayout(contentLayout)

        layout.addWidget(contentFrame, 0, 1)

        self.setLayout(layout)
