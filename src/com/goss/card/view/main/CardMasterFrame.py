'''
Created on Apr 2, 2010

@author: Aaron Goss
'''
from PyQt5 import QtCore, QtWidgets, QtGui

from com.goss.card.view.card.CardUIFrame import CardUIFrame
from com.goss.card.view.menu.CardMenuBar import CardMenuBar
from com.goss.card.view.card.tree.DockableTree import DockableTree
from com.goss.card.controller.card.CardCommand import CardCommand


class CardMasterFrame(QtWidgets.QMainWindow):

    def __init__(self, onClose, category, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args)
        self.command = CardCommand()
        self.onClose = onClose
        self.menuBar = self.createMenuBar()
        self.statusBar = self.createStatusBar()
        self.mainWindow = self.createMainWindow()

        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, DockableTree(self))
        self.setCentralWidget(self.mainWindow)
        self.setMenuBar(self.createMenuBar())
        self.setupWindow()

        if category not in ("", None):
            self.command.populateCards(category)

    def setupWindow(self):
        self.setWindowTitle("Re-Collections")
        self.setWindowIcon(QtGui.QIcon("/home/zarniwoop/workspace/Flashcards/libs/trayicon_alert.png"))

    def createMenuBar(self):
        """
        createToolBar()
        
        This function creates the toolbar in which this application is driven.  There are a number
        of sub-widgets which can be swapped in/out; this menu will drive that process.  This function
        returns a new instance of QMenuBar.
        """
        return CardMenuBar()

    def createStatusBar(self):
        """
        createStatusBar()
        
        This function creates the statusbar along the bottom of the window.  This status bar will contain
        basic user-messages for understanding the overall context of the applciation.  This function
        returns a new instance of QStatusBar.
        """

    def createMainWindow(self):
        """
        createMainWindow()
        
        This function creates the main window in which all card-content for this application will appear.
        This window is not specified, as it pertains to layout or functionality.  It is up to the rest of
        the application to create the correct windows for whatever options the user has requested.  This 
        function returns a new instance of QDockWidget.
        """

        return CardUIFrame()

    def closeEvent(self, event):
        event.ignore()
        self.onClose.emit()


class CardTableModel(QtCore.QAbstractTableModel):
    def __init__(self, datain, headerdata, parent=None, *args):
        """ datain: a list of lists
            headerdata: a list of strings
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.arraydata = datain
        self.headerdata = headerdata

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        return len(self.arraydata[0])

    def data(self, index, role):
        if not index.isValid():
            return QtCore.QVariant()
        elif role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()

        value = self.arraydata[index.row()][index.column()]

        if index.column() == 0:
            return QtCore.QVariant(value)
        else:
            return QtCore.QVariant(value)
