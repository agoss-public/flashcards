"""
Created on May 22, 2010

This class forms the bulk of the launching capacity for the flashcard app.  This
class allows the program to open an icon in the system tray when it has detected
flashcards which should be displayed to the user.  If the user chooses, they can
ignore the icon, or force it to sleep and go away for a specified amount of time.

@author: Aaron Goss
"""
import os
import sqlite3
import sys
import time

from PyQt5 import QtWidgets, QtCore, QtGui

from com.goss.card.model.card.db.DBConnection import DBConnection
from com.goss.card.view.help.CardAboutDialog import CardAboutDialog
from com.goss.card.view.main.CardMasterFrame import CardMasterFrame

DB_PATH = os.getcwd() + "/db/flashcards.db"
DB_CHECK_PERIOD = 5
SLEEP_VAL = 0
SLEEP_VALS = {"5 seconds": 5,
              "10 seconds": 10,
              "15 seconds": 15,
              "5 minutes": 300,
              "Quick Sleep": 900,
              "15 minutes": 900,
              "30 minutes": 1800,
              "1 hour": 3600,
              "2 hours": 7200}

SYS_TRAY_ICON = "/home/zarniwoop/workspace/Flashcards/libs/trayicon.png"
SYS_ALERT_ICON = "/home/zarniwoop/workspace/Flashcards/libs/trayicon_alert.png"


class CardLauncher(QtWidgets.QWidget):
    """
    This class launches the icon in the system tray.  This class executes a simple
    QThread which periodically checks the DB for cards which need to be viewed.  It
    connects the Timer with the slot in the tray icon so it can update the status of
    the icon based on whether or not cards are found in the DB
    """

    def __init__(self, parent):
        """
        Initialize the Card Launcher and kick off the DB timer
        """
        QtWidgets.QWidget.__init__(self, parent)
        self.trayIcon = CardTrayIcon(parent)
        self.iconTimer = TrayIconTimer(self.trayIcon.showTrayIcon)


class TrayIconTimer(QtCore.QThread):
    """
    This class executes as a QThread, polling the DB on regular intervals to
    see if any cards are ready to be presented to the user.  If so, the Tray
    Icon class is communicated with via a signal/slot mechanism.
    """

    updateIcon = QtCore.pyqtSignal("bool")

    def __init__(self, iconSlot, launchThread=True):
        """
        Initializes the state of the polling thread and starts it.  This method
        will immediately start this thread unless the calling class explicitly 
        disables this process.  This is a convenience method for me since I sometimes
        forget to kick things off :)
        """
        QtCore.QThread.__init__(self)
        self.isRunning = False
        self.iconIsVisible = False
        self.updateIcon.connect(iconSlot)
        if launchThread:
            self.start()

    def run(self):
        """
        This function iteratively checks the database on a period set by the user.
        If the user chooses not to sleep, the default interval is chosen.  Otherwise,
        this thread sleeps until the user-specified time.
        """
        self.isRunning = True
        nextCheck = 0
        global SLEEP_VAL

        while self.isRunning:
            time.sleep(DB_CHECK_PERIOD)

            # Only check the DB if the icon isn't displayed
            if self.isRunning and not self.iconIsVisible:
                nextCheck += DB_CHECK_PERIOD

                if nextCheck >= SLEEP_VAL:
                    self.updateIcon.emit(self.checkDatabase())
                    nextCheck = 0

    def checkDatabase(self):
        """
        This function runs a query to see if there are any flashcards which should have been shown to the
        user already.
        """
        conn = sqlite3.connect(DB_PATH)
        cursor = conn.cursor()
        cursor.execute(''.join(["SELECT last_displayed_time, display_interval, category FROM flashcards,",
                                "buckets WHERE flashcards.bucket_id == buckets.id"]))
        currentDates = []
        for timestamp in cursor:
            timeToDisplay = float(timestamp[0]) + float(timestamp[1])
            if timeToDisplay <= time.time():
                currentDates.append(timeToDisplay)
        conn.close()

        return len(currentDates) > 0

    def shutdown(self):
        self.isRunning = False  # Force thread to stop


class CardTrayIcon(QtWidgets.QSystemTrayIcon):
    """
    This class sets up the visible icon in the system tray.  This
    icon contains several nested menus which, when displayed, allow
    the user to sleep the GUI thread, hiding the tray icon until the
    sleep period has ended.
    """

    hideIcon = QtCore.pyqtSignal("bool")
    closeGui = QtCore.pyqtSignal()

    def __init__(self, widget):
        """
        Initializes the tray icon and sets up all context menus
        """
        QtWidgets.QSystemTrayIcon.__init__(self, QtGui.QIcon(SYS_TRAY_ICON), widget)
        self.widget = widget
        self.cardGui = None
        self.paused = False

        self.hideIcon.connect(self.showTrayIcon)
        self.closeGui.connect(self.onCloseGui)

        self.setContextMenu(self.createMenus(widget))
        self.setToolTip("Right-click for Flashcard Options")
        self.show()

    def createMenus(self, widget):
        """
        Creates sub-menus and QActions for sleep-options in the 
        tray icon.  These menus/actions allow users to specify
        intervals in which this app will lie dormant.
        """
        menu = QtWidgets.QMenu(widget)
        menu.aboutToShow.connect(self.menuDisplayed)
        menu.aboutToHide.connect(self.menuClosed)

        # Setup actions
        launchAction = menu.addAction("Open...")
        catMenu = menu.addAction("Open Category")
        menu.addSeparator()
        quickSleepAction = menu.addAction("&Quick Sleep")
        sleepAction = menu.addAction("&Sleep")
        menu.addSeparator()
        helpAction = menu.addAction("Help...")
        aboutAction = menu.addAction("&About")
        menu.addSeparator()
        exitAction = menu.addAction("E&xit")

        # Create sub-menus
        catMenu.setMenu(self.createCategoryMenus())
        sleepAction.setMenu(self.createIntervalMenus())

        # Setup triggers
        quickSleepAction.triggered.connect(self.sleep)
        aboutAction.triggered.connect(self.showAbout)
        helpAction.triggered.connect(self.showHelp)
        exitAction.triggered.connect(self.exit)
        launchAction.triggered.connect(self.launchFlashcards)

        # Setup other attributes of menus
        quickSleepAction.setToolTip("Sleep for 15 minutes")

        return menu

    def createCategoryMenus(self):
        """
        Creates sub-menus and QActions which allow the user to
        specify the categories of flash cards they want to view.
        This allows the user to focus on certain groups of cards
        if they prefer.
        """
        categoryMenu = QtWidgets.QMenu(self.widget)

        conn = DBConnection.getConnection(DBConnection.ConnectionTypes.SQLITE_CONN)
        cursor = conn.cursor()
        cursor.execute(''.join(["SELECT category FROM flashcards,categories ",
                                "WHERE flashcards.category == categories.name"]))

        for category in cursor:
            categoryMenu.addAction(category[0])

        categoryMenu.triggered.connect(self.launchFlashcardsWithCategory)

        return categoryMenu

    def createHelpMenus(self):
        """
        Creates a help menu w/sub-options to open various dialogs
        """
        menu = QtWidgets.QMenu(self.widget)

        return menu

    def createIntervalMenus(self):
        """
        Iterates across the SLEEP_VALS keys and
        creates menu options for each.
        """
        menu = QtWidgets.QMenu(self.widget)

        for string in SLEEP_VALS.keys():
            if string.find("Quick") == -1:
                menu.addAction(string)

        menu.triggered.connect(self.sleep)
        return menu

    @QtCore.pyqtSlot(bool)
    def showTrayIcon(self, bool):
        """
        Slot which allows external threads to modify the visibility of the
        tray icon.  Signals which will communicate with this slot need to
        emit a boolean so we can determine the visibility status of the
        icon.  The icon will only be updated if the pause flag is False.
        """
        if bool and not self.paused:
            self.setIcon(QtGui.QIcon(SYS_ALERT_ICON))
            self.setToolTip("There are cards to view")
        else:
            self.setIcon(QtGui.QIcon(SYS_TRAY_ICON))
            self.setToolTip("Right-click for Flashcard Options")

    @QtCore.pyqtSlot(list)
    def updateCardMenu(self, categories):
        """
        Slot which allows us to ask the tray icon to create a new set of
        menus containing the categories listed from the query
        """
        self.createCategoryMenus(categories)

    @QtCore.pyqtSlot(QtWidgets.QAction)
    def launchFlashcardsWithCategory(self, action):
        if type(action) == QtWidgets.QAction:
            self.launchFlashcards(action.text())
        else:
            self.launchFlashcards()

    @QtCore.pyqtSlot()
    def launchFlashcards(self, card=""):
        self.hideIcon.emit(False)
        self.cardGui = CardMasterFrame(self.closeGui, card, self.widget)
        self.cardGui.show()

    @QtCore.pyqtSlot()
    def onCloseGui(self):
        self.cardGui.hide()
        self.cardGui = None

    @QtCore.pyqtSlot(bool)
    @QtCore.pyqtSlot(QtWidgets.QAction)
    def sleep(self, passedObj):
        global SLEEP_VAL

        if type(passedObj) == QtWidgets.QAction:
            SLEEP_VAL = SLEEP_VALS[str(passedObj.text())]
        else:
            SLEEP_VAL = 900
        self.hideIcon.emit(False)

    @QtCore.pyqtSlot(bool)
    def showAbout(self, bool):
        help = CardAboutDialog(self.widget)
        help.exec_()

    @QtCore.pyqtSlot(bool)
    def showHelp(self, bool):
        pass

    @QtCore.pyqtSlot()
    def menuDisplayed(self):
        self.hideIcon.emit(False)
        self.paused = True

    @QtCore.pyqtSlot()
    def menuClosed(self):
        self.paused = False

    @QtCore.pyqtSlot()
    def exit(self):
        sys.exit()
