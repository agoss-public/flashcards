"""
Created on May 29, 2010

@author: Aaron Goss
"""
from PyQt5 import QtWidgets


class TrayIconMenuFactory():
    class Menus():
        menus = (HELP, SLEEP, CATEGORY, CONFIG) = range(0, 4)

    """
    Helper class for generating different menus for
    the tray icons 
    """

    def getMenu(self, type, parent, slot):
        if type not in TrayIconMenuFactory.Menus.menus:
            raise Exception("Could not find %s menu type" % type)
        elif type == TrayIconMenuFactory.Menus.HELP:
            return self.createHelpMenu(slot)
        elif type == TrayIconMenuFactory.Menus.SLEEP:
            return self.createSleepMenu(slot)
        elif type == TrayIconMenuFactory.Menus.CATEGORY:
            return self.createCategoryMenu(slot)
        elif type == TrayIconMenuFactory.Menus.CONFIG:
            return self.createConfigMenu(slot)

    def createHelpMenu(self, parent, slot):
        menu = QtWidgets.QMenu(parent)
        menu.triggered.connect(slot)
        return menu

    def createSleepMenu(self, parent, slot):
        menu = QtWidgets.QMenu(parent)
        menu.triggered.connect(slot)
        return menu

    def createCategoryMenu(self, parent, slot):
        menu = QtWidgets.QMenu(parent)
        menu.triggered.connect(slot)
        return menu

    def createConfigMenu(self, parent, slot):
        menu = QtWidgets.QMenu(parent)
        menu.triggered.connect(slot)
        return menu
